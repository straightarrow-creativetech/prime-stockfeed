<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'stockfeed_wp_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dOj<!ra}Is-ZqV-mX|pIPaBA{mE;l+&Ddq]%+BS;5<^3H]1>:0Bj|36;b}{$+f!0' );
define( 'SECURE_AUTH_KEY',  'FYVmEG0q}]sH]v6(gjWoc$|ZYqKbN-ch1j<?3[(vxd<%=A0q%qqqkHy*iPnv2#dX' );
define( 'LOGGED_IN_KEY',    '1Yp&CKguZ(LvAGioY_e:4oo6*L8vQ#!<II fVL#AQ[D`>;yQkeLu?rkn3PMsit7c' );
define( 'NONCE_KEY',        'w&w[G|QA$=fQC38H~b H5 G,L?h-l.>Hgw.R4(KzBpLNir~+,s_yW?DS~^kO13#G' );
define( 'AUTH_SALT',        '[ROg!ve^2fy:oH#g~JqKGp+-8ej=tJy3m/3$.k+WCd_:_LT=Fy=GiM|^v9J3?]@V' );
define( 'SECURE_AUTH_SALT', 'mQ-;5!66Kx`*v*r35$/I9]|c[&EyJEB:RL_1KFp~7lhVbpdW7,4P%eaJMM} aQP:' );
define( 'LOGGED_IN_SALT',   '+~*+fN7`EWTeOqA2kIEMDQ56J/-;(x<1#59X?,JsjP*(TixltfVM(9-e+&k]I_*?' );
define( 'NONCE_SALT',       'Jj(WB%a3zk~_Ttv/+7.T#BdOvv0J,,#<,H<s}%PoC^S]?<gZ}_NL8kqQq6=HghUd' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
