<?php get_header(); ?>
<?php if(have_rows('flexible_modules')): ?>
	<?php
	while(have_rows('flexible_modules')):
		the_row();
	?>
		<?php
		if(get_row_layout() == 'banner'):
			$banner_image = get_sub_field('banner_image');
			$banner_heading = get_sub_field('banner_heading');
			$banner_cta_copy = get_sub_field('banner_cta_copy');
			$banner_cta = get_sub_field('banner_cta');
		?>
		<div class="banner" style="background-image:url(<?php echo $banner_image['url']; ?>);">
			<div class="container">
				<div class="row align-items-end justify-content-center">
					<div class="col-lg-10">
						<h1><?php echo $banner_heading; ?></h1>
						<p><a class="text-uppercase" href="<?php echo $banner_cta; ?>"><?php echo $banner_cta_copy; ?></a></p>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'heading_image_and_copy_padded'):
			$section_background_color = get_sub_field('section_background_color');
			$section_background_image = get_sub_field('section_hic_background_image');
			$section_heading = get_sub_field('section_heading');
			$section_image = get_sub_field('section_image');
			$section_copy = get_sub_field('section_copy');
		?>
		<div class="heading_image_and_copy_padded" style="background-color:<?php echo $section_background_color; ?>; background-image:url(<?php echo $section_background_image['url']; ?>);">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="row">
							<div class="col">
								<h2><?php echo $section_heading; ?></h2>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-4 order-2 order-md-1">
								<img src="<?php echo $section_image['url']; ?>">
							</div>
							<div class="col-lg-8 order-1 order-md-2">
								<?php echo $section_copy; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'image_and_copy_padded'):
			$section_background_color = get_sub_field('section_background_color');
			$section_background_image = get_sub_field('section_ic_background_image');
			$section_image = get_sub_field('section_image');
			$section_copy = get_sub_field('section_copy');
		?>
		<div class="image_and_copy_padded" style="background-color:<?php echo $section_background_color; ?>; background-image:url(<?php echo $section_background_image['url']; ?>);">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="row align-items-center">
							<div class="col-lg-6">
								<img src="<?php echo $section_image['url']; ?>">
							</div>
							<div class="col-lg-6">
								<?php echo $section_copy; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'product_categories'):
			$category = get_sub_field('category');
			$section_intro = get_sub_field('intro_copy');
			$section_copy = get_sub_field('section_copy');
			$section_background_color = get_sub_field('section_background_color');
		?>
		<div class="product_categories" style="background-color:<?php echo $section_background_color; ?>;">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<?php echo $section_intro; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<?php if($category): ?>
						<div class="row categories text-center">
							<?php
							$count = 1;

							foreach($category as $cat):
								$cat_image = get_field('category_image', $cat);

								if($count == 1){
									$col_margin_class = 'marginB-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 2){
									$col_margin_class = 'marginB-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 3){
									$col_margin_class = 'marginB-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 4){
									$col_margin_class = 'marginT-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 5){
									$col_margin_class = 'marginT-15';
									$row_margin_class = 'marginLR-0';
								}
							?>
							<div class="col-lg-4 <?php echo $col_margin_class; ?>">
								<div class="row align-items-center justify-content-center category-box <?php echo $row_margin_class; ?>" style="background-image:url(<?php echo $cat_image['url']; ?>);">
									<div class="col">
										<p class="category-name"><?php echo $cat->name; ?></p>
									</div>
									<div class="category-overlay">
										<div class="row align-items-center">
											<div class="col">
												<p><?php echo $cat->description; ?></p>
												<p><a class="text-uppercase" href="<?php echo get_term_link($cat); ?>">View Products</a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
								$count++;
							endforeach;
							?>
							<div class="col-lg-4 marginT-15">
								<div class="row align-items-center justify-content-center category-box marginLR-0" style="background-image:url(<?php echo get_template_directory_uri().'/assets/images/logo-pink.png'; ?>);">
									<div class="col">
										<p class="category-name"><a href="#" data-toggle="modal" data-target="#enquiry-form">Enquire Now</a></p>
									</div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<div class="row justify-content-center">
							<div class="col-lg-10">
								<?php echo $section_copy; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'copy_and_image_full'):
			$section_heading = get_sub_field('section_heading');
			$section_copy = get_sub_field('section_copy');
			$section_cta_copy = get_sub_field('section_cta_copy');
			$section_cta = get_sub_field('section_cta');
			$section_image = get_sub_field('section_image_right');
			$section_background_color = get_sub_field('section_background_color');
		?>
		<div class="copy_and_image_full" style="background-color:<?php echo $section_background_color; ?>;">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-lg-6 copy-full">
						<div class="row justify-content-end">
							<div class="col-lg-10 paddingR-80">
								<h3><?php echo $section_heading; ?></h3>
								<?php echo $section_copy; ?>
								<?php if($section_cta): ?>
								<p class="section-cta"><a class="text-uppercase" href="<?php echo $section_cta; ?>"><?php echo $section_cta_copy; ?></a></p>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="col-lg-6 image-full" style="background-image:url(<?php echo $section_image['url']; ?>);">
						&nbsp;
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'image_and_copy_full'):
			$section_heading = get_sub_field('section_heading');
			$section_copy = get_sub_field('section_copy');
			$section_cta_copy = get_sub_field('section_cta_copy');
			$section_cta = get_sub_field('section_cta');
			$section_image = get_sub_field('section_image_left');
			$section_background_color = get_sub_field('section_background_color');
		?>
		<div class="image_and_copy_full" style="background-color:<?php echo $section_background_color; ?>;">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-lg-6 image-full order-2 order-md-1" style="background-image:url(<?php echo $section_image['url']; ?>);">
						&nbsp;
					</div>
					<div class="col-lg-6 copy-full order-1 order-md-2">
						<div class="row justify-content-start">
							<div class="col-lg-10 paddingL-80">
								<h3><?php echo $section_heading; ?></h3>
								<?php echo $section_copy; ?>
								<?php if($section_cta): ?>
								<p class="section-cta"><a class="text-uppercase" href="<?php echo $section_cta; ?>"><?php echo $section_cta_copy; ?></a></p>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
<div class="pink-wrapper">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9">
				<?php
				if(have_posts()):
					while(have_posts()):
					the_post();
				?>
				<?php the_content(); ?>
				<?php
					endwhile;
				endif;

				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_template_part('template-parts/modal', 'enquiry'); ?>
<?php get_footer(); ?>