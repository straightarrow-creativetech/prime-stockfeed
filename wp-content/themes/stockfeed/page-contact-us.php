<?php
/* Template Name: Contact */

get_header();

if(have_posts()):
	while(have_posts()):
		the_post();
?>
<div class="pink-wrapper contact-us">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
				<?php echo do_shortcode(get_field('contact_form_shortcode')); ?>
			</div>
		</div>
	</div>
</div>
<?php if(get_field('office_address_heading')): ?>
<div class="white-wrapper">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9">
				<div class="row">
					<div class="col-lg-12">
						<h3><?php the_field('office_address_heading'); ?></h3>
					</div>
				</div>
				<div class="row align-items-end">
					<div class="col-lg-6">
						<p><?php the_field('office_phone'); ?></p>
						<p><?php the_field('physical_address'); ?></p>
					</div>
					<div class="col-lg-6">
						<p><?php the_field('mailing_address'); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
	endwhile;
endif;

wp_reset_postdata();

get_footer();
?>