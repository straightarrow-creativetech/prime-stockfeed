<div class="modal fade" id="enquiry-form" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<img src="<?php echo get_template_directory_uri().'/assets/images/close-dark.svg'; ?>">
				</button>
			</div>
			<div class="modal-body">
				<h2>Enquire Now</h2>
				<p>Leave your details below and a team member will be in touch</p>
				<?php echo do_shortcode('[contact-form-7 id="140" title="Enquiry Form"]'); ?>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="enquiry-confirm" tabindex="-1">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<img src="<?php echo get_template_directory_uri().'/assets/images/close-dark.svg'; ?>">
				</button>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2>Thank you for your enquiry</h2>
							<p>A team member will be in touch</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>