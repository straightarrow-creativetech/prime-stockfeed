<?php
/* Template Name: Product Categories */

get_header();

if(have_posts()):
	while(have_posts()):
		the_post();
?>
<div class="pink-wrapper">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
		<?php
		$category = get_terms('product_category', ['hide_empty' => false]);

		if($category):
		?>
		<div class="row categories text-center">
			<?php
			$count = 1;
			$order = [];

			foreach($category as $cat){
				$cat_image = get_field('category_image', $cat);
				$cat_order = get_field('category_order', $cat);
				$order[$cat_order] = [
					'name' => $cat->name,
					'description' => $cat->description,
					'link' => get_term_link($cat),
					'image' => $cat_image['url']
				];
			}
			
			ksort($order);

			foreach($order as $key => $cat):
				if($key == 1){
					$col_margin_class = 'marginB-15';
					$row_margin_class = 'marginLR-0';
				}
				elseif($key == 2){
					$col_margin_class = 'marginB-15';
					$row_margin_class = 'marginLR-0';
				}
				elseif($key == 3){
					$col_margin_class = 'marginB-15';
					$row_margin_class = 'marginLR-0';
				}
				elseif($key == 4){
					$col_margin_class = 'marginT-15';
					$row_margin_class = 'marginLR-0';
				}
				elseif($key == 5){
					$col_margin_class = 'marginT-15';
					$row_margin_class = 'marginLR-0';
				}
			?>
			<div class="col-lg-4 <?php echo $col_margin_class; ?>">
				<div class="row align-items-center justify-content-center category-box <?php echo $row_margin_class; ?>" style="background-image:url(<?php echo $cat['image']; ?>);">
					<div class="col">
						<p class="category-name"><?php echo $cat['name']; ?></p>
					</div>
					<div class="category-overlay">
						<div class="row align-items-center">
							<div class="col">
								<p><?php echo $cat['description']; ?></p>
								<a class="text-uppercase" href="<?php echo $cat['link']; ?>">View Products</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
				$count++;
			endforeach;
			?>
			<div class="col-lg-4 marginT-15">
				<div class="row align-items-center justify-content-center category-box marginLR-0" style="background-image:url(<?php echo get_template_directory_uri().'/assets/images/logo-pink.png'; ?>);">
					<div class="col">
						<p class="category-name"><a href="">Enquire Now</a></p>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>
<?php if(have_rows('flexible_modules')): ?>
	<?php
	while(have_rows('flexible_modules')):
		the_row();
	?>
		<?php
		if(get_row_layout() == 'banner'):
			$banner_image = get_sub_field('banner_image');
			$banner_heading = get_sub_field('banner_heading');
			$banner_cta_copy = get_sub_field('banner_cta_copy');
			$banner_cta = get_sub_field('banner_cta');
		?>
		<div class="banner" style="background-image:url(<?php echo $banner_image['url']; ?>);">
			<div class="container">
				<div class="row align-items-end justify-content-center">
					<div class="col-lg-10">
						<h1><?php echo $banner_heading; ?></h1>
						<a class="text-uppercase" href="<?php echo $banner_cta; ?>"><?php echo $banner_cta_copy; ?></a>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'heading_image_and_copy_padded'):
			$section_background_color = get_sub_field('section_background_color');
			$section_background_image = get_sub_field('section_hic_background_image');
			$section_heading = get_sub_field('section_heading');
			$section_image = get_sub_field('section_image');
			$section_copy = get_sub_field('section_copy');
		?>
		<div class="heading_image_and_copy_padded" style="background-color:<?php echo $section_background_color; ?>; background-image:url(<?php echo $section_background_image['url']; ?>);">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="row">
							<div class="col">
								<h2><?php echo $section_heading; ?></h2>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-lg-4">
								<img src="<?php echo $section_image['url']; ?>">
							</div>
							<div class="col-lg-8">
								<?php echo $section_copy; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'image_and_copy_padded'):
			$section_background_color = get_sub_field('section_background_color');
			$section_background_image = get_sub_field('section_ic_background_image');
			$section_image = get_sub_field('section_image');
			$section_copy = get_sub_field('section_copy');
		?>
		<div class="image_and_copy_padded" style="background-color:<?php echo $section_background_color; ?>; background-image:url(<?php echo $section_background_image['url']; ?>);">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="row align-items-center">
							<div class="col-lg-6">
								<img src="<?php echo $section_image['url']; ?>">
							</div>
							<div class="col-lg-6">
								<?php echo $section_copy; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'image_and_copy_unpadded'):
			$section_background_color = get_sub_field('section_background_color');
			$section_background_image = get_sub_field('section_ic_background_image');
			$section_image = get_sub_field('section_image');
			$section_copy = get_sub_field('section_copy');
		?>
		<div class="image_and_copy_unpadded" style="background-color:<?php echo $section_background_color; ?>; background-image:url(<?php echo $section_background_image['url']; ?>);">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="row align-items-center">
							<div class="col-lg-6">
								<img src="<?php echo $section_image['url']; ?>">
							</div>
							<div class="col-lg-6">
								<?php echo $section_copy; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'product_categories'):
			$category = get_sub_field('category');
			$section_copy = get_sub_field('section_copy');
			$section_background_color = get_sub_field('section_background_color');
		?>
		<div class="product_categories" style="background-color:<?php echo $section_background_color; ?>;">
			<div class="container">
				<div class="row text-center">
					<div class="col-lg-12">
						<h2>Products</h2>
						<?php if($category): ?>
						<div class="row categories">
							<?php
							$count = 1;

							foreach($category as $cat):
								$cat_image = get_field('category_image', $cat);

								if($count == 1){
									$col_margin_class = 'marginB-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 2){
									$col_margin_class = 'marginB-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 3){
									$col_margin_class = 'marginB-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 4){
									$col_margin_class = 'marginT-15';
									$row_margin_class = 'marginLR-0';
								}
								elseif($count == 5){
									$col_margin_class = 'marginT-15';
									$row_margin_class = 'marginLR-0';
								}
							?>
							<div class="col-lg-4 <?php echo $col_margin_class; ?>">
								<div class="row align-items-center justify-content-center category-box <?php echo $row_margin_class; ?>" style="background-image:url(<?php echo $cat_image['url']; ?>);">
									<div class="col">
										<p class="category-name"><?php echo $cat->name; ?></p>
									</div>
									<div class="category-overlay">
										<div class="row align-items-center">
											<div class="col">
												<p><?php echo $cat->description; ?></p>
												<a class="text-uppercase" href="<?php echo get_term_link($cat); ?>">View Products</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
								$count++;
							endforeach;
							?>
							<div class="col-lg-4 marginT-15">
								<div class="row align-items-center justify-content-center category-box marginLR-0" style="background-image:url(<?php echo get_template_directory_uri().'/assets/images/logo-pink.png'; ?>);">
									<div class="col">
										<p class="category-name"><a href="">Enquire Now</a></p>
									</div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<div class="row justify-content-center">
							<div class="col-lg-10">
								<?php echo $section_copy; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'copy_and_image_full'):
			$section_heading = get_sub_field('section_heading');
			$section_copy = get_sub_field('section_copy');
			$section_cta_copy = get_sub_field('section_cta_copy');
			$section_cta = get_sub_field('section_cta');
			$section_image = get_sub_field('section_image_right');
			$section_background_color = get_sub_field('section_background_color');
		?>
		<div class="copy_and_image_full" style="background-color:<?php echo $section_background_color; ?>;">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-lg-6 copy-full">
						<div class="row justify-content-end">
							<div class="col-lg-10 paddingR-80">
								<h3><?php echo $section_heading; ?></h3>
								<?php echo $section_copy; ?>
								<?php if($section_cta): ?>
								<a class="text-uppercase" href="<?php echo $section_cta; ?>"><?php echo $section_cta_copy; ?></a>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="col-lg-6 image-full" style="background-image:url(<?php echo $section_image['url']; ?>);">
						&nbsp;
					</div>
				</div>
			</div>
		</div>
		<?php
		elseif(get_row_layout() == 'image_and_copy_full'):
			$section_heading = get_sub_field('section_heading');
			$section_copy = get_sub_field('section_copy');
			$section_cta_copy = get_sub_field('section_cta_copy');
			$section_cta = get_sub_field('section_cta');
			$section_image = get_sub_field('section_image_left');
			$section_background_color = get_sub_field('section_background_color');
		?>
		<div class="image_and_copy_full" style="background-color:<?php echo $section_background_color; ?>;">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-lg-6 image-full order-sm-1" style="background-image:url(<?php echo $section_image['url']; ?>);">
						&nbsp;
					</div>
					<div class="col-lg-6 copy-full order-sm-2">
						<div class="row justify-content-start">
							<div class="col-lg-10 paddingL-80">
								<h3><?php echo $section_heading; ?></h3>
								<?php echo $section_copy; ?>
								<?php if($section_cta): ?>
								<a class="text-uppercase" href="<?php echo $section_cta; ?>"><?php echo $section_cta_copy; ?></a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>
<?php
	endwhile;
endif;

wp_reset_postdata();

get_footer();
?>