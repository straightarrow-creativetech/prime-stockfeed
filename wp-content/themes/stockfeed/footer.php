<?php wp_footer(); ?>
	<footer id="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<?php wp_nav_menu(['theme_location' => 'footer-category-menu']); ?>
				</div>
				<div class="col-lg-3">
					<?php wp_nav_menu(['theme_location' => 'header-menu']); ?>
				</div>
				<div class="col-lg-3">
					<p><?php echo get_field('phone_number', 'options'); ?></p>
					<p><?php echo get_field('physical_address', 'options'); ?></p>
				</div>
				<div class="col-lg-3 text-right">
					<a href="<?php echo home_url(); ?>">
						<?php if(get_field('logo', 'options')['url']): ?>
						<img src="<?php echo get_field('logo', 'options')['url']; ?>">
						<?php else: ?>
						<img src="<?php echo get_template_directory_uri().'/assets/images/logo.svg'; ?>">
						<?php endif; ?>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 text-right paddingT-50">
					<p>&copy;2021 Prime Stockfeeds. All Rights Reserved | <a href="">Sitemap</a> | <a href="<?php echo get_privacy_policy_url(); ?>">Privacy Policy</a></p>
				</div>
			</div>
		</div>
	</footer>
	<footer id="mobile-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<a href="<?php echo home_url(); ?>">
						<?php if(get_field('logo', 'options')['url']): ?>
						<img src="<?php echo get_field('logo', 'options')['url']; ?>">
						<?php else: ?>
						<img src="<?php echo get_template_directory_uri().'/assets/images/logo.svg'; ?>">
						<?php endif; ?>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<p><?php echo get_field('phone_number', 'options'); ?></p>
					<p><?php echo get_field('physical_address', 'options'); ?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<p>&copy;2021 Prime Stockfeeds.</p>
					<p>All Rights Reserved</p>
					<p><a href="">Sitemap</a> | <a href="<?php echo get_privacy_policy_url(); ?>">Privacy Policy</a></p>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>