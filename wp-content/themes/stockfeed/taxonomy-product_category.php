<?php
get_header();

$cat_name = get_queried_object()->name;
$cat_desc = get_queried_object()->description;
$cat_slug = get_queried_object()->slug;
$args = [
	'post_type' 	=> 'products',
	'post_status' 	=> 'publish',
	'order'			=> 'ASC',
	'tax_query' 	=> [[
		'taxonomy' 	=> 'product_category',
		'field' 	=> 'slug',
		'terms' 	=> $cat_slug
	]]
];
$products = new WP_Query($args);
?>
<div class="pink-wrapper">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10">
				<h1><?php echo $cat_name; ?></h1>
				<p><?php echo $cat_desc; ?></p>
				<p>Not all product ranges are displayed graphically <a class="tax-enquire-btn" href="#" data-toggle="modal" data-target="#enquiry-form">Enquire Now</a> if you would like to know more.</p>
			</div>
		</div>
	</div>
	<div class="product-boxes">
		<div class="container">
			<?php
			if($products->have_posts()):
				while($products->have_posts()):
					$products->the_post();
			?>
			<div class="row product-box">
				<div class="col-lg-2" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>); background-repeat: no-repeat; background-size: cover;">
					&nbsp;
				</div>
				<div class="col-lg-10 product-info">
					<div class="row">
						<div class="col-lg-9">
							<div class="row">
								<div class="col-lg-12">
									<h3><?php the_title(); ?></h3>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<p>Stage:</p>
									<p><?php the_field('stage'); ?></p>
								</div>
								<div class="col-lg-4">
									<p>Age:</p>
									<p><?php the_field('age'); ?></p>
								</div>
								<div class="col-lg-4">
									<p>Order Code:</p>
									<p><?php the_field('order_code'); ?></p>
								</div>
							</div>
						</div>
						<div class="col-lg-3 enquire-now">
							<button type="button" class="btn btn-link btn-enquire" data-product="<?php the_title(); ?>" data-toggle="modal" data-target="#enquiry-form">Enquire Now</button>
						</div>
					</div>
				</div>
			</div>
			<?php
				endwhile;
			endif;

			wp_reset_postdata();
			?>
		</div>
	</div>
</div>
<div class="image_and_copy_full" style="background-color:#D0E0D8;">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-lg-6 image-full order-sm-1" style="background-image:url(<?php echo get_template_directory_uri().'/assets/images/bg-contact.png'; ?>);">
				&nbsp;
			</div>
			<div class="col-lg-6 copy-full order-sm-2">
				<div class="row justify-content-start">
					<div class="col-lg-10 paddingL-80">
						<h3>Contact</h3>
						<p>Comments or questions about which feeding solution is right for your birds? We’re here to provide expert nutritional advice. Call us now – or email us using our contact form anytime – and we’ll get back to you as soon as we can.</p>
						<a class="text-uppercase" href="<?php echo home_url().'/contact'; ?>">Contact Us</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part('template-parts/modal', 'enquiry'); ?>
<?php get_footer(); ?>