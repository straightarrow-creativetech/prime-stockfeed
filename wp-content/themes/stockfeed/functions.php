<?php

function register_ps_menus()
{
    register_nav_menus([
        'header-menu' => __('Header Menu'),
        'footer-category-menu' => __('Footer Category Menu'),
        'mobile-menu' => __('Mobile Menu')
    ]);
}

function ps_scripts()
{
    $cache_bust = '2021033020211015';
    
	wp_enqueue_style('bootstrap-style', get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), '4.6.0');
	wp_enqueue_style('ps-style', get_template_directory_uri().'/assets/css/styles.css', array(), $cache_bust);

	wp_enqueue_script('bootstrap-script', get_template_directory_uri().'/assets/js/bootstrap.min.js', array('jquery'), '4.6.0', true);
	wp_enqueue_script('ps-script', get_template_directory_uri().'/assets/js/script.js', array(), $cache_bust, true);
}

function product_category()
{
    register_taxonomy(
        'product_category',
        'products',
        array(
            'hierarchical' => true,
            'label' => 'Product Categories',
            'public' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'product_category',
                'with_front' => false
            )
        )
    );
}

function ps_custom_products_post_type()
{
	$labels = array(
		'name'                  => 'Products',
		'singular_name'         => 'Product',
		'menu_name'             => 'Products',
		'name_admin_bar'        => 'Product',
		'archives'              => 'Products Archives',
		'attributes'            => 'Product Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Products',
		'add_new_item'          => 'Add New Product',
		'add_new'               => 'Add New Product',
		'new_item'              => 'New Product',
		'edit_item'             => 'Edit Product',
		'update_item'           => 'Update Product',
		'view_item'             => 'View Product',
		'view_items'            => 'View Products',
		'search_items'          => 'Search Product',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into Product',
		'uploaded_to_this_item' => 'Uploaded to this Product',
		'items_list'            => 'Testimonials list',
		'items_list_navigation' => 'Testimonials list navigation',
		'filter_items_list'     => 'Filter Testimonials list',
	);
	$args = array(
		'label'                 => 'Product',
		'description'           => 'Post Type Description',
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'thumbnail', 'custom-fields', 'revisions'),
		'taxonomies'            => array('product_category'),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-carrot',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type('products', $args);
}

function set_custom_edit_products_columns($columns)
{
    $columns = [
    	'cb'				=> $columns['cb'],
    	'title'				=> 'Product Name',
    	'product_category'	=> 'Category',
    	'stage'				=> 'Stage',
    	'age'				=> 'Age',
    	'order_code'		=> 'Order Code',
    	'date'				=> 'Published Date'
    ];

    return $columns;
}

function custom_products_column($column, $post_id)
{
	switch($column){
		case 'product_category':
			$terms = get_the_term_list($post_id , 'product_category' , '' , ',' , '');

			if(is_string($terms))
				echo $terms;
			else
				_e('Unable to get author(s)', 'your_text_domain');
			break;

		case 'stage':
			echo get_field('stage', $post_id);
			break;

		case 'age':
			echo get_field('age', $post_id);
			break;

		case 'order_code':
			echo get_field('order_code', $post_id);
			break;
	}
}

function cf7_select_dropdown($tag, $unused)
{
	if($tag['name'] != 'stockfeed-products'){
		return $tag;
	}

	$args = [
		'post_type' 	=> 'products',
		'numberposts'	=> -1
	];
	$products = get_posts($args);

	foreach($products as $product){
		$tag['raw_values'][] = $product->post_title;
		$tag['labels'][] = $product->post_title;
	}

	$pipes = new WPCF7_Pipes($tag['raw_values']);
	$tag['values'] = $pipes->collect_befores();
	$tag['pipes'] = $pipes;

	return $tag;
}

if(function_exists('acf_add_options_page')){
	acf_add_options_page([
		'page_title' 	=> 'Prime Stockfeed General Info',
		'menu_title'	=> 'Prime Stockfeed General Info',
		'menu_slug' 	=> 'ps-general-info',
		'capability'	=> 'edit_posts',
		'position'		=> 2,
		'redirect'		=> false
	]);
}

add_theme_support('post-thumbnails');
add_action('init', 'register_ps_menus');
add_action('wp_enqueue_scripts', 'ps_scripts');
add_action('init', 'product_category');
add_action('init', 'ps_custom_products_post_type', 0);
add_filter('manage_products_posts_columns', 'set_custom_edit_products_columns');
add_action('manage_products_posts_custom_column' , 'custom_products_column', 10, 2);
add_filter('wpcf7_form_tag', 'cf7_select_dropdown', 10, 2);