<!DOCTYPE html>
<html>
	<head>
		<title><?php wp_title('|', true); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri().'/assets/favicons/apple-touch-icon.png'; ?>">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri().'/assets/favicons/favicon-32x32.png'; ?>">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri().'/assets/favicons/favicon-16x16.png'; ?>">
		<link rel="manifest" href="<?php echo get_template_directory_uri().'/assets/favicons/site.webmanifest'; ?>">
		<link rel="mask-icon" href="<?php echo get_template_directory_uri().'/assets/favicons/safari-pinned-tab.svg'; ?>" color="#005051">
		<meta name="msapplication-TileColor" content="#00aba9">
		<meta name="theme-color" content="#ffffff">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<header id="main-nav" <?php echo (!is_front_page()) ? 'class="header-white"' : ''; ?> >
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="logo text-center">
							<a href="<?php echo home_url(); ?>">
								<?php if(get_field('logo', 'options')['url']): ?>
								<img src="<?php echo get_field('logo', 'options')['url']; ?>">
								<?php else: ?>
								<img src="<?php echo get_template_directory_uri().'/assets/images/logo.svg'; ?>">
								<?php endif; ?>
							</a>
							<div id="triangle-topleft"></div>
							<div id="triangle-topright"></div>
						</div>
					</div>
					<div class="col-lg-9 text-right">
						<?php wp_nav_menu(['theme_location' => 'header-menu']); ?>
					</div>
				</div>
			</div>
		</header>
		<header id="mobile-nav">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="logo text-center">
							<a href="<?php echo home_url(); ?>">
								<?php if(get_field('logo', 'options')['url']): ?>
								<img src="<?php echo get_field('logo', 'options')['url']; ?>">
								<?php else: ?>
								<img src="<?php echo get_template_directory_uri().'/assets/images/logo.svg'; ?>">
								<?php endif; ?>
							</a>
							<div id="triangle-topleft"></div>
							<div id="triangle-topright"></div>
						</div>
					</div>
					<div class="col-sm-6 text-right paddingA-20 burger-menu">
						<a class="btn-mob-menu" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri().'/assets/images/hamburger.svg'; ?>"></a>
					</div>
				</div>
			</div>
			<div class="mob-menu">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-right paddingT-20 paddingR-20 paddingL-20">
							<a class="btn-close-mob-menu" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri().'/assets/images/close.svg'; ?>"></a>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 text-center">
							<?php wp_nav_menu(['theme_location' => 'mobile-menu', 'menu_id' => 'menu-header-mobile']); ?>
						</div>
					</div>
					<div class="row align-items-end">
						<div class="col-sm-12 text-center">
							<a href="<?php echo home_url(); ?>">
								<?php if(get_field('logo', 'options')['url']): ?>
								<img src="<?php echo get_field('logo', 'options')['url']; ?>">
								<?php else: ?>
								<img src="<?php echo get_template_directory_uri().'/assets/images/logo.svg'; ?>">
								<?php endif; ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</header>
		<?php if(!is_front_page()): ?>
		<div class="header-pad"></div>
		<?php endif; ?>