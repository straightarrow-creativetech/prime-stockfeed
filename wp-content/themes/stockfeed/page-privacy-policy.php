<?php
/* Template Name: Privacy Policy */

get_header();

if(have_posts()):
	while(have_posts()):
		the_post();
?>
<div class="pink-wrapper privacy-policy">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-9">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</div>
<?php
	endwhile;
endif;

wp_reset_postdata();

get_footer();
?>