var $ = jQuery;

$(function(){
	$(window).on('scroll', function(e){
		var scroll = $(window).scrollTop();

		if(scroll == 0){
			$('#main-nav').css('background-color', 'transparent');
			$('#main-nav').removeClass('shadow');
			$('#main-nav .menu-header-container').css({'padding-top': '40px', 'padding-bottom': '40px'});
		}
		else {
			$('#main-nav').css('background-color', '#ffffff');
			$('#main-nav').addClass('shadow');
			$('#main-nav .menu-header-container').css({'padding-top': '20px', 'padding-bottom': '20px'});
		}
	});

	$('#menu-header > li').last().children('a').attr('data-toggle', 'modal').attr('data-target', '#enquiry-form');
	$('#menu-header > li').last().children('a').on('click', function(e){
		e.preventDefault();

		$('#productName').hide();
	});

	if($('.categories').length){
		$('.categories .category-box').last().find('a').on('click', function(e){
			e.preventDefault();

			$('#productName').hide();
		});
	}

	if($('.tax-enquire-btn').length){
		$('.tax-enquire-btn').on('click', function(e){
			e.preventDefault();

			$('#productName').hide();
		});
	}

	$('.btn-mob-menu').on('click', function(e){
		e.preventDefault();

		$('#mobile-nav').css('height', '100vh');
		$('.mob-menu').css('left', '0');
		$('body').css('overflow-y', 'hidden');
	});

	$('.btn-close-mob-menu').on('click', function(e){
		e.preventDefault();

		$('.mob-menu').css('left', '100%');
		$('body').css('overflow-y', '');
		$('#mobile-nav').css('height', '140px');
	});

	if($('.product-box').length){
		$('.btn-enquire').on('click', function(e){
			$('#productName').val($(this).data('product'));
			$('#productName').show();
		});
	}

	$('.wpcf7').on('wpcf7submit', function(e){
		$('#enquiry-form').modal('hide');
		$('#enquiry-confirm').modal('show');
	});
});